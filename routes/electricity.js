const express = require("express");
const router = express.Router();
const mysql = require("mysql");

// const cors = require('cors')
//
// const corsOptions = {
//     origin: 'https://suivipfeapi.inchtechs.com',
//     optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
// }

const connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "electricity",
});

router.get("/", function (req, res) {
  connection.query(
    "SELECT " +
      "u.login, " +
      "DAY(p.time) AS jour, " +
      "p.time, " +
      "HOUR(p.time) AS heure, " +
      "AVG(p.current_1) AS current_1," +
      "AVG(p.current_2) AS current_2, " +
      "AVG(p.energie_1) AS energie_1, " +
      "AVG(p.energie_2) AS energie_2, " +
      "AVG(p.voltage) AS voltage " +
      "FROM parameters p JOIN User u " +
      "ON p.id_user_id = u.id GROUP by jour, heure ORDER BY jour,heure DESC",
    function (err, results) {
      if (err) {
        console.log(err);
      } else {
        let fixedData = {};
        let hoursEcountered = [];
        console.log(hoursEcountered);
        JSON.parse(JSON.stringify(results)).forEach((data) => {
          let date = data.time;
          let year = date.split("T")[0].split("-")[0];
          let month = date.split("T")[0].split("-")[1];
          let day = date.split("T")[0].split("-")[2];
          let heure = date.split("T")[1].split(":")[0];
          let uniqueHourId = date.split(":")[0];
          if (fixedData[year]) {
            if (fixedData[year][month]) {
              if (fixedData[year][month][day]) {
                if (fixedData[year][month][day].hours.indexOf(heure) >= 0) {
                  //{hours:[],current_1:[],current_2:[]}
                  // if(hoursEcountered.indexOf(uniqueHourId)>=0){
                  let position =
                    fixedData[year][month][day].hours.indexOf(heure);

                  //calculating data current_1;
                  let prev_current_1_value =
                    fixedData[year][month][day].current_1[position];
                  let new_current_1_value =
                    (prev_current_1_value + data.current_1) / 2;

                  //calculating data current_2;
                  let prev_current_2_value =
                    fixedData[year][month][day].current_2[position];
                  let new_current_2_value =
                    (prev_current_2_value + data.current_2) / 2;

                  //calculating data energie_1;
                  let prev_energie_1_value =
                    fixedData[year][month][day].energie_1[position];
                  let new_energie_1_value =
                    (prev_energie_1_value + data.energie_1) / 2;

                  //calculating data energie_2;
                  let prev_energie_2_value =
                    fixedData[year][month][day].energie_2[position];
                  let new_energie_2_value =
                    (prev_energie_2_value + data.energie_2) / 2;

                  //obtain sequence of values
                  let fdY = fixedData[year];
                  let fdM = fixedData[year][month];
                  let fdD = fixedData[year][month][day];

                  //affectation
                  let current_1 = fixedData[year][month][day].current_1;
                  current_1[position] = new_current_1_value;
                  fdD.current_1 = current_1;

                  //affectation
                  let current_2 = fixedData[year][month][day].current_2;
                  current_1[position] = new_current_2_value;
                  fdD.current_2 = current_2;

                  //affectation
                  let energie_1 = fixedData[year][month][day].energie_1;
                  energie_1[position] = new_energie_1_value;
                  fdD.energie_1 = energie_1;

                  //affectation
                  let energie_2 = fixedData[year][month][day].energie_2;
                  energie_2[position] = new_energie_2_value;
                  fdD.energie_2 = energie_2;

                  fdM[day] = fdD;
                  fdY[month] = fdM;
                  fixedData[year] = fdY;
                  // }
                } else {
                  fixedData[year][month][day].hours.push(heure);
                  fixedData[year][month][day].current_1.push(data.current_1);
                  fixedData[year][month][day].current_2.push(data.current_2);
                  fixedData[year][month][day].energie_1.push(data.energie_1);
                  fixedData[year][month][day].energie_2.push(data.energie_2);
                }
              } else {
                //all are present
                fixedData[year][month][day] = {
                  hours: [heure],
                  current_1: [data.current_1],
                  current_2: [data.current_2],
                  energie_1: [data.energie_1],
                  energie_2: [data.energie_2],
                };
              }
            } else {
              //no day but month and year present
              fixedData[year][month] = [
                {
                  [day]: {
                    hours: [heure],
                    current_1: [data.current_1],
                    current_2: [data.current_2],
                    energie_1: [data.energie_1],
                    energie_2: [data.energie_2],
                  },
                },
              ];
            }
          } else {
            //no day but month and year present
            fixedData[year] = {
              [month]: {
                [day]: {
                  hours: [heure],
                  current_1: [data.current_1],
                  current_2: [data.current_2],
                  energie_1: [data.energie_1],
                  energie_2: [data.energie_2],
                },
              },
            };
          }
          hoursEcountered.push(uniqueHourId);
        });
        res.send(fixedData);
      }
      //     const energy1 = results.map(en => {
      //         // console.log(en.energie_1)
      //         return {
      //             energie_1: en.energie_1,
      //             mois: en.mois,
      //             jour: en.jour,
      //             heure: en.heure
      //         }
      //     })
      //     res.send(energy1)
    }
  );
});
router.get("/status", function (req, res) {
  connection.query(
    "SELECT etat_1, etat_2 FROM status ORDER BY date DESC limit 1",
    function (err, status) {
      if (err) {
        console.log(err);
      } else {
        res.send(status);
      }
    }
  );
});

// TODO: change method of the route under for get
router.post("/status", function (req, res) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader("Access-Control-Allow-Credentials", "true");
  res.setHeader("Access-Control-Max-Age", "1800");
  res.setHeader("Access-Control-Allow-Headers", "content-type");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "PUT, POST, GET, DELETE, PATCH, OPTIONS"
  );

  const { body } = req;
  const status = JSON.parse(body.body);
  console.log(status.etat_1);
  console.log(status.etat_2);

  connection.query(
    "INSERT INTO status (etat_1, etat_2, user_id, date) VALUES(?,?,1,current_time())",
    [status.etat_1, status.etat_2],
    function (err, rows) {
      if (err) {
        console.log(err);
      } else {
        console.log("insert done!!!");
      }
    }
  );
});

router.get("/percent", function (req, res) {
  connection.query(
    "SELECT price, energie_totale, sum(energie_1) AS en_1, sum(energie_2) AS en_2 FROM parameters GROUP BY price ",
    function (err, price) {
      if (err) {
        console.log(err);
      } else {
        res.send(price);
      }
    }
  );
});

module.exports = router;
