const express = require("express");
const router = express.Router();
const mysql = require("mysql");

// const connection = mysql.createConnection({
//   host: 'localhost',
//   user: 'inso4656_smartfarming',
//   password: 'SmartFarming@21',
//   database: 'inso4656_smartFarming'
// })

const connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "smartfarming",
});

/* GET home page. */
router.get("/", function (req, res) {
  connection.query(
    " SELECT m.nom_maladie, m.conseil, m.img, m.date_maladie, s.temp, s.hum, s._date FROM maladie m JOIN " +
      "(SELECT id_user, temp, hum, _date  FROM sensors ORDER BY _date DESC limit 1) " +
      "as s ON s.id_user = m.id_user ORDER BY 4 DESC limit 1",
    function (err, results) {
      if (err) {
        console.log(err);
      } else {
        res.send(results);
      }
    }
  );
});

module.exports = router;
