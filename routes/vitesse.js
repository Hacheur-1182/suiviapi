const express = require("express");
const router = express.Router();
const mysql = require("mysql");

// const connection = mysql.createConnection({
//   host: 'localhost',
//   user: 'inso4656_exces_vitesse',
//   password: 'Vitesse@groupe13',
//   database: 'inso4656_exces_vitesse'
// })

const connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "vitesse",
});

router.get("/", function (req, res) {
  connection.query("SELECT * FROM parameters", function (error, results) {
    if (error) throw error;
    res.send(JSON.stringify(results));
  });
});
router.get("/user", function (req, res) {
  connection.query("SELECT login, password FROM user", function (err, user) {
    if (err) console.log(err);
    res.send(JSON.stringify(user));
  });
});

module.exports = router;
