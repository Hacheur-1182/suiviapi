const express = require("express");
const router = express.Router();
const mysql = require("mysql");

const connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "assistance_driver",
});

router.get("/", function (req, res) {
  connection.query(
    "SELECT DATE(_date) AS date, TIME(_date) AS heure, latitude AS latitude, longitude AS longitude FROM track",
    function (err, results) {
      if (err) {
        throw err;
      } else {
        res.send(results);
      }
    }
  );
});

module.exports = router;
