const express = require("express");
const router = express.Router();
const mysql = require("mysql");

const connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "gesconso2",
});

router.get("/", function (req, res) {
  // const month = JSON.stringify(new Date())
  // const getMonth = month.split("-")
  connection.query(
    "SELECT u.login," +
      "YEAR(p._time) AS year," +
      "Month(p._time) AS mois, " +
      "DAY(p._time) AS day, " +
      "HOUR(p._time) AS heure, " +
      "sum(p.energie) AS sum_energy " +
      "FROM parameters p " +
      "JOIN user u ON p.user_id = u.id GROUP BY mois ORDER BY mois ASC",
    function (err, params) {
      if (err) {
        console.log(err);
      } else {
        res.send(params);
      }
    }
  );
});

router.post("/", function (req, res) {
  const { body } = req;
  console.log(body);
  // connection.query("INSERT INTO historical VALUES ("+body+")")
});

module.exports = router;
