const express = require("express");
const mysql = require("mysql");
const router = express.Router();

const connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "waterquality",
});

router.get("/", function (req, res) {
  connection.query(
    "SELECT DAY(date) AS date,HOUR(date) AS hour, AVG(phvalue) AS phvalue,AVG(temp) AS temp FROM parameters GROUP BY 1,2",
    function (err, results) {
      if (err) {
        console.log(err);
        res.send(err);
      } else {
        res.send(results);
      }
    }
  );
});

module.exports = router;
