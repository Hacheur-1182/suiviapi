const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");
const logger = require("morgan");
const cors = require("cors");
const bodyParser = require("body-parser");

const smartFarmingRouter = require("./routes/smartFarming");
const usersRouter = require("./routes/users");
const electricityRouter = require("./routes/electricity");
const energyRouter = require("./routes/energy");
const waterQualityRouter = require("./routes/waterquality");
const assistanceDriver = require("./routes/assistance_driver");
const vitesse = require("./routes/vitesse");

const app = express();

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors());

app.use("/", smartFarmingRouter);
app.use("/electricity", electricityRouter);
app.use("/energy", energyRouter);
app.use("/waterquality", waterQualityRouter);
app.use("/assistancedriver", assistanceDriver);
app.use("/vitesse", vitesse);
app.use("/users", usersRouter);

module.exports = app;
